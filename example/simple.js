'use strict';

const map = {
  '.ico': 'image/x-icon',
  '.html': 'text/html',
  '.js': 'text/javascript',
  '.json': 'application/json',
  '.css': 'text/css',
  '.png': 'image/png',
  '.jpg': 'image/jpeg',
  '.wav': 'audio/wav',
  '.mp3': 'audio/mpeg',
  '.svg': 'image/svg+xml',
  '.pdf': 'application/pdf',
  '.doc': 'application/msword',
  '.fx' : 'application/fx',
  '.babylon': 'application/babylon',
  '.babylonmeshdata': 'application/babylonmeshdata'
};

const options = {
  authorization: null,
  pathname: '/primus',
  parser: 'binary', // 'JSON', // use 'binary'
  transformer: 'websockets',
  plugin: {},
  pingInterval: 30000,
  global: 'Primus',
  compression: false,
  maxLength: 10485760,
  transport: {},
  idGenerator: undefined,
  origins: '*',  methods: ['GET', 'HEAD', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
  credentials: true,
  maxAge: '30 days',
  headers: '',
  exposed: false
};

const http = require('http');
const url = require("url");
const path = require('path');
const fs = require('fs');
const Primus = require('primus');
const Terser = require("terser");

const webroot = path.join(process.cwd(), 'public');

async function requestHandler(req, res) {
  // parse URL
  const parsedUrl = url.parse(req.url);

  // extract URL path
  let { pathname } = parsedUrl;
  if (pathname === '/') pathname = '/index.html';

  const ext = path.parse(pathname).ext;
  const requestedFile = path.join(webroot, pathname);

  fs.exists(requestedFile, exist => {
    if (!exist) {
      // if the file is not found, return 404
      res.statusCode = 404;
      res.end(`File ${requestedFile} not found!`);
      return;
    }

    // if is a directory search for index file matching the extension
    fs.stat(requestedFile, (err, stats) => {
      if (err) {
        res.statusCode = 500;
        res.end(`Invalid request`);
        return;
      }

      if (stats.isDirectory()) {
        res.statusCode = 500;
        res.end(`Invalid request`);
        return;
      }

      res.setHeader('Content-Type', map[ext] || 'text/plain');

      const rstream = fs.createReadStream(requestedFile);
      rstream.pipe(res).on('error', (err) => {
        res.statusCode = 500;
        res.end(`Error getting the file: ${err}.`);
      });
    });
  });
}

const server = http.createServer(requestHandler).listen(8080);
const primus = new Primus(server, options);
const clientLibrary = primus.library();
const minified = Terser.minify(clientLibrary);
const writeStream = fs.createWriteStream(path.join(process.cwd(), 'public', 'ws.min.js'));
writeStream.write(minified.code, 'utf8');

primus.on('connection', function (spark) {
  // spark is the new connection.
  const { headers, address, query, socket, id, request, write } = spark;

  spark.on('data', () => {});

  spark.on('end', () => {});
});

primus.on('disconnection', function (spark) {
  // the spark that disconnected
});
