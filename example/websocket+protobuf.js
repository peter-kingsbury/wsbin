'use strict';

const map = {
  '.ico': 'image/x-icon',
  '.html': 'text/html',
  '.js': 'text/javascript',
  '.json': 'application/json',
  '.css': 'text/css',
  '.png': 'image/png',
  '.jpg': 'image/jpeg',
  '.wav': 'audio/wav',
  '.mp3': 'audio/mpeg',
  '.svg': 'image/svg+xml',
  '.pdf': 'application/pdf',
  '.doc': 'application/msword',
  '.fx' : 'application/fx',
  '.babylon': 'application/babylon',
  '.babylonmeshdata': 'application/babylonmeshdata',
  '.proto': 'application/protobuf'
};

const options = {
  authorization: null,
  pathname: '/primus',
  parser: 'binary', // 'binary', // 'JSON', // use 'binary'
  transformer: 'websockets',
  plugin: {},
  pingInterval: 1000,
  global: 'Primus',
  compression: false,
  maxLength: 10485760,
  transport: {},
  idGenerator: function() {
    return generate(alphabet, ID_LENGTH);
  },
  origins: '*',  methods: ['GET', 'HEAD', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
  credentials: true,
  maxAge: '30 days',
  headers: '',
  exposed: false
};

const http = require('http');
const url = require("url");
const path = require('path');
const fs = require('fs');
const Primus = require('primus');
const Terser = require("terser");
const Protobuf = require('protobufjs');
const generate = require('nanoid/generate');
const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
const ID_LENGTH = 16;

const webroot = path.join(process.cwd(), 'public');

async function requestHandler(req, res) {
  // parse URL
  const parsedUrl = url.parse(req.url);

  // extract URL path
  let { pathname } = parsedUrl;
  if (pathname === '/') pathname = '/index.html';

  const ext = path.parse(pathname).ext;
  let requestedFile = null;

  if (ext === '.proto') {
    requestedFile = path.join(process.cwd(), 'proto', pathname);
  } else {
    requestedFile = path.join(webroot, pathname);
  }

  fs.exists(requestedFile, exist => {
    if (!exist) {
      // if the file is not found, return 404
      res.statusCode = 404;
      res.end(`File ${requestedFile} not found!`);
      return;
    }

    // if is a directory search for index file matching the extension
    fs.stat(requestedFile, (err, stats) => {
      if (err) {
        res.statusCode = 500;
        res.end(`Invalid request`);
        return;
      }

      if (stats.isDirectory()) {
        res.statusCode = 500;
        res.end(`Invalid request`);
        return;
      }

      res.setHeader('Content-Type', map[ext] || 'text/plain');

      const rstream = fs.createReadStream(requestedFile);
      rstream.pipe(res).on('error', (err) => {
        res.statusCode = 500;
        res.end(`Error getting the file: ${err}.`);
      });
    });
  });
}

function startWebsocketServer() {
  const server = http.createServer(requestHandler).listen(8080);
  const primus = new Primus(server, options);
  const clientLibrary = primus.library();
  const minified = Terser.minify(clientLibrary);
  const writeStream = fs.createWriteStream(path.join(process.cwd(), 'public', 'ws.min.js'));
  writeStream.write(minified.code, 'utf8');

  return { server, primus };
}

function sendMessage(spark, payload, type) {
  const errMsg = type.verify(payload);
  if (errMsg)
    throw Error(errMsg);

  const message = type.fromObject(payload);

  const buffer = type.encode(message).finish();

  console.log(`Sending buffer of length ${buffer.length} bytes`);

  spark.write(buffer);
}

const PacketType = Object.freeze({
  NONE: 0,
  PING: 1,
  PONG: 2,
  ENTITY_UPDATE: 3
});

(async () => {

  const protoPath = path.join(process.cwd(), 'proto', 'packet.proto');
  const root = await Protobuf.load(protoPath);
  // const Hello = root.lookupType('Hello');
  const EntityUpdate = root.lookupType('EntityUpdate');

  const { server, primus } = startWebsocketServer();

  primus.on('connection', async function (spark) {
    // spark is the new connection.
    const { headers, address, query, socket, id, request, write } = spark;

    setInterval(() => {
      /*const payload = {
        type: 0,
        id: generate(alphabet, ID_LENGTH),
        message: 'Hello, world!'
      };*/

      const payload = {
        type: PacketType.ENTITY_UPDATE,
        entityId: generate(alphabet, ID_LENGTH),
        position: {
          x: 10,
          y: 20,
          z: 30
        },
        orientation: {
          x: 0,
          y: 1,
          z: 0,
          w: 1
        }
      };

      sendMessage(spark, payload, EntityUpdate);
    }, 1000);

    spark.on('data', () => {});

    spark.on('end', () => {});
  });

  primus.on('disconnection', function (spark) {
    // the spark that disconnected
  });

})();
