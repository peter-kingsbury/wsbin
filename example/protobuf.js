'use strict';

const path = require('path');
const Protobuf = require('protobufjs');
const generate = require('nanoid/generate');
const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
const ID_LENGTH = 16;

(async () => {
  const protoPath = path.join(process.cwd(), 'proto', 'entity.proto');
  const root = await Protobuf.load(protoPath);
  const Entity = root.lookupType('Entity');

  const payload = {
    id: generate(alphabet, ID_LENGTH),
    posx: 1.0000001,
    posy: 2.0000002,
    posz: 3.0000003,
    orix: 4.0000004,
    oriy: 5.0000005,
    oriz: 6.0000006
  };

  const errMsg = Entity.verify(payload);
  if (errMsg)
    throw Error(errMsg);

  const message = Entity.fromObject(payload);

  const buffer = Entity.encode(message).finish();

  console.log(`Length=${buffer.length}`, buffer);

  const decoded = Entity.decode(buffer);

  console.log(decoded);
})();
