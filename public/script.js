async function boot() {
  console.log('It works!');

  const proto = await protobuf.load('/packet.proto');
  const { EntityUpdate } = proto;

  var url = 'ws://localhost:8080';
  var options = {
    reconnect: {
      max: Infinity,
      min: 500,
      retries: 10,
      reconnectTimeout: 10000,
      factor: 2
    },
    timeout: 5000,
    pingTimeout: 5000,
    strategy: [ 'online', 'timeout', 'disconnect' ],
//  manual: true,
    websockets: true,
    network: true,
    transport: {},
    queueSize: Infinity,
    //parser: 'binary'
  };
  var primus = new Primus({ options });

// primus.on('outgoing::reconnect', () => { console.debug('outgoing::reconnect'); });
// primus.on('reconnect scheduled', () => { console.debug('reconnect scheduled'); });
// primus.on('reconnect',           () => { console.debug('reconnect'); });
// primus.on('reconnected',         () => { console.debug('reconnected'); });
// primus.on('reconnect timeout',   () => { console.debug('reconnect timeout'); });
// primus.on('reconnect failed',    () => { console.debug('reconnect failed'); });
// primus.on('timeout',             () => { console.debug('timeout'); });
// primus.on('outgoing::open',      () => { console.debug('outgoing::open'); });
// primus.on('incoming::open',      () => { console.debug('incoming::open'); });
  primus.on('open',                () => { console.debug('open'); });
// primus.on('destroy',             () => { console.debug('destroy'); });
// primus.on('incoming::error',     () => { console.debug('incoming::error'); });
  primus.on('error',               () => { console.debug('error'); });
//primus.on('incoming::data',      (data) => { debugger; data = String.fromCharCode.apply(null, new Uint8Array(data.data)); console.debug('incoming::data', data); });
// primus.on('incoming::data',      (data) => { console.log(data); });
// primus.on('outgoing::data',      () => { console.debug('outgoing::data'); });
// primus.on('incoming::end',       () => { console.debug('incoming::end'); });
// primus.on('outgoing::end',       () => { console.debug('outgoing::end'); });
  primus.on('end',                 () => { console.debug('end'); });
  primus.on('close',               () => { console.debug('close'); });
// primus.on('incoming::ping',      () => { console.debug('incoming::ping'); });
// primus.on('outgoing::pong',      () => { console.debug('outgoing::pong'); });
  primus.on('online',              () => { console.debug('online'); });
  primus.on('offline',             () => { console.debug('offline'); });
  primus.on('readyStateChange',    () => { console.debug('readyStateChange'); });
// primus.on('outgoing::url',       () => { console.debug('outgoing::url'); });
  primus.on('data',                (data) => {
    data = EntityUpdate.decode(new Uint8Array(data));
    console.log(data);
  });
}
