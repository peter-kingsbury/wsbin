# wsbin

A protobuf-enabled, binary-websocket-brandishing webserver.

## Getting Started

1. Clone the repo
1. Run `npm start`
1. Visit [http://localhost:8080](http://localhost:8080)

## Nano ID Alphabet

```javascript
const generate = require('nanoid/generate');
const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
generate(alphabet, 16); // vhUTYNpVGUyKyxpG
```

## References

gRPC Basics for Node.js - https://grpc.io/docs/tutorials/basic/node/
Protocol Buffers - https://developers.google.com/protocol-buffers/docs/overview
Pack/Unpack UUID - https://gist.github.com/ksss/902c30f04cee540ad940ce8f57c2ffbe
Nano ID Dictionary - https://github.com/CyberAP/nanoid-dictionary
Nano ID Collision Calculator - https://zelark.github.io/nano-id-cc/
protobuf.js - https://protobufjs.github.io/protobuf.js/#using-proto-files
